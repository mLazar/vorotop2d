////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *           in Two Dimensions            *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: functions.cc


#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sys/stat.h>

#include "filters.hh"
#include "variables.hh"



////////////////////////////////////////////////////
////
////   PRINT HELP MESSAGE WITH BASIC INSTRUCTIONS
////
////////////////////////////////////////////////////

void help_message(void) {
    std::cout <<
    "   ********************************************\n"
    "   *                                          *\n"
    "   *     VoroTop2D: Voronoi Cell Topology     *\n"
    "   *    Visualization and Analysis Toolkit    *\n"
    "   *            in Two Dimensions             *\n"
    "   *              (Version 1.0)               *\n"
    "   *                                          *\n"
    "   *            Emanuel A. Lazar              *\n"
    "   *          Bar-Ilan University             *\n"
    "   *             September 2022               *\n"
    "   *                                          *\n"
    "   ********************************************\n"
    "                                               \n"
    "Syntax: VoroTop <filename> [options]           \n"
    "                                               \n"
    "         VoroTop reads a LAMMPS dump input file and computes the complete Voronoi\n"
    "         cell topology for each point.  Output is determined by options.         \n"
    "                                                                                 \n"
    "                                                                                 \n"
    "Available options:                                                               \n"
    "                                                                                 \n"
    " -f    : Generate a LAMMPS <filename>.dump file using a filter provided by user  \n"
    "                                                                                 \n"
    " -p    : Compute p-vector for each particle, save to file <filename>.pvectors.   \n"
    " -d    : Compute distribution of p-vectors, save to file <filename>.distribution.\n"
    " -g    : Compute distribution of p-vectors for perturbations of a system. This   \n"
    "         option should be followed by two numbers, one specifying the number of  \n"
    "         samples, the other specifying the size of the random perturbations.     \n"
    "                                                                                 \n"
    " -c    : Cluster analysis.  Particles with topological types not included in the \n"
    "         specified filter are considered to be defects.  Defect particles with   \n"
    "         adjacent Voronoi cells are considered to belong to the same defect; we  \n"
    "         call each Voronoi-contiguous set of defect particles a cluster and      \n"
    "         assign the constituent particles a unique index.  Similar cluster       \n"
    "         analysis is also performed for types in the filter.  This option        \n"
    "         requires loading a filter file for analysis.                            \n"
    "                                                                                 \n"
    " -u    : Compute and output unnormalized Voronoi pair correllation data.         \n"
    " -v    : Compute and output normalized Voronoi pair correllation data.           \n"
    "                                                                                 \n"
    " -e    : Output eps illustration of the system.  An optional number follows,     \n"
    "         indicating the number of particles that should be included in the       \n"
    "         image.  This is helpful when the system is large, and it is sufficient  \n"
    "         to view only a part of it.                                              \n"
    " -r    : Change the particle radius by this multiplicative factor.               \n"
    " -n    : Do not draw Voronoi cells; only draw particles.                         \n"
    "                                                                                 \n"
    " -o    : Specify output file primary name <filename>.  If requested, p-vectors   \n"
    "         will be saved to <filename>.pvectors; distribution will be saved to     \n"
    "         <filename>.distribution; LAMMPS data will be saved to <filename>.dump.  \n"
    " -od   : Specify output directory.  If requested, output files will be saved to  \n"
    "         specified directory, using current filename; directory must previously  \n"
    "         exist.                                                                  \n"
    "                                                                                 \n";
}



////////////////////////////////////////////////////
////
////   PARSE INPUT ARGUMENTS AND DETERMINE WHAT
////   SHOULD BE DONE
////
////////////////////////////////////////////////////

void parse_arguments(int argc, char *argv[])
{
    if(argc<2)
    {
        help_message();
        std::cerr << "No input data file specified.\n\n\n";
        exit(-1);
    }
    
    if(argc<3)
    {
        help_message();
        std::cerr << "No command-line options specified.\n\n\n";
        exit(-1);
    }
    
    // DETERMINE INPUT AND OUTPUT FILENAMES
    name_of_data_file = argv[1];
    std::size_t found = name_of_data_file.find_last_of("/\\");
    std::string path  = name_of_data_file.substr(0,found+1);
    std::string file  = name_of_data_file.substr(found+1);
    
    // DEFAULT THREADS: 1
#ifdef _OPENMP
    if(omp_get_max_threads()>2) threads = omp_get_max_threads()-2;
    else                        threads = 1;
#else
    threads = 1;
#endif
    
    // FIRST ARGUMENT (d=1) IS INPUT DATA FILENAME; PARSE REMAINING ARGUMENTS.
    for(int d=2; d<argc; d++)
    {
        if(strcmp(argv[d],"-f")==0)                        // SPECIFY FILTER FILE
        {
            if(argc > d+1 && argv[d+1][0]!='-')
            {
                f_switch=1;
                particle_coloring_scheme=3;                         // IF SYSTEM WILL BE DRAWN
                filename_pfilter = argv[d+1];
                d++;
            }
            else
            {
                help_message();
                std::cout << "-f option indicated but no filter specified.\n";
                exit(0);
            }
        }

        else if(strcmp(argv[d],"-p") ==0)  p_switch=1;     // PRINT OUT P-VECTORS FOR EACH PARTICLE
        else if(strcmp(argv[d],"-d") ==0)  d_switch=1;     // PRINT OUT DISTRIBUTION OF P-VECTORS OF SYSTEM

        
        // THIS SHOULD TAKE 0, 1, OR 2 ARGUMENTS.  IF 0, THEN DEFFAULT COLORING.  IF 1, THEN COLOR SCHEME
        // AND DRAW ENTIRE SYSTEM.  IF 2, THEN COLOR SCHEME AND DRAW THAT NUMBER OF PARTICLES.
        
        else if(strcmp(argv[d],"-e")==0)                   // OUTPUT EPS FILE; TAKES 0, 1, OR 2 ARGUMENTS.
        {                                                  // 0 ARGUMENTS: DEFAULT COLOR SCHEME, DRAW ENTIRE SYSTEM
            e_switch=1;                                    // 1 ARGUMENT : COLOR SCHEME, DRAW ENTIRE SYSTEM
            if(argc > d+1 && argv[d+1][0]!='-')            // 2 ARGUMENTS: COLOR SCHEME, DRAW GIVEN NUMBER OF PARTICLES
            {
                particle_coloring_scheme = atoi(argv[d+1]);         // FIRST OPTIONAL ARGUMENT GIVES COLOR SCHEME
                
                if(argc > d+2 && argv[d+2][0]!='-')        // SECOND ARGUMENT, DETERMINES NUMBER OF PARTICLES
                {
                    particles_in_eps = atoi(argv[d+2]);
                    d++;
                }
                
                d++;
            }
            else                                           // NO ARGUMENTS
            {
                if(particle_coloring_scheme==-1)           // IF COLORING SCHEME WAS SET BY -f SWITCH, THEN DON'T DEFAULT TO 2
                particle_coloring_scheme  = 2;             // DEFAULT particle_coloring_scheme (2) COLOR BY EDGE COUNT
                particles_in_eps = 0;                      // DRAW ALL PARTICLES
            }
            
            // CATCH ERROR IN COLOR SCHEME, OUTPUT INSTRUCTIONS
            if(particle_coloring_scheme<0 || particle_coloring_scheme>6)
            {
                std::cout << "Coloring scheme is " << particle_coloring_scheme << '\n';
                exit(0);
            }
            if(r_switch==0) radius_scaling = 1.;
        }
        else if(strcmp(argv[d],"-n") ==0)  n_switch=1;     // DO NOT DRAW VORONOI CELLS; ONLY DRAW PARTICLES

        else if(strcmp(argv[d],"-u")==0)                   // OUTPUT UNNORMALIZED VORONOI PAIR CORRELATION FUNCTION DATA
        {                                                  // TAKES OPTIONAL ARGUMENT TO DETERMINE MAXIMAL RADIUS.
            u_switch = 1;
            if(argc > d+1 && argv[d+1][0]!='-')
            {
                max_radius = atoi(argv[d+1]);
                d++;
            }
            else                                           // DEFAULT VALUE max_radius = 20
                max_radius = 20;
        }

        else if(strcmp(argv[d],"-v")==0)                   // OUTPUT NORMALIZED VORONOI PAIR CORRELATION FUNCTION DATA
        {                                                  // TAKES OPTIONAL ARGUMENT TO DETERMINE MAXIMAL RADIUS.
            v_switch = 1;
            if(argc > d+1 && argv[d+1][0]!='-')
            {
                max_radius = atoi(argv[d+1]);
                d++;
            }
            else                                           // DEFAULT VALUE max_radius = 20
                max_radius = 20;
        }
        
        // COMPUTE DISTRIBUTION FOR PERTURBATIONS OF A GIVEN SYSTEM
        else if(strcmp(argv[d],"-g")==0)
        {
            if(d+1 == argc || d+2 == argc)     // LAST OPTION
            {
                // WE HAVE TROUBLE SINCE THIS MEANS WE DON'T HAVE
                // TWO MORE VALUES, WHICH WOULD BE THE MIN AND
                // MAX VALUES WE ARE SEEKING
                help_message();
                std::cout << "-g option indicated; must be followed by two numbers, samples and perturbation.\n";
                exit(0);
            }
            else if(argv[d+1][0]=='-' || argv[d+2][0]=='-')
            {
                help_message();
                std::cout << "-g option indicated; must be followed by two numbers, samples and perturbation.\n";
                exit(0);
            }
            else  // AT THIS POINT WE HAVE TWO OBJECTS FOLLOWING.  WE DETERMINE THESE VALUES
                  // AND MAKE SURE THAT THEY ARE VALUES BETWEEN 0 AND 1; IF THEY ARE WE USE
                  // THEM, OTHERWISE WE RETURN AN ERROR MESSAGE.
            {
                g_switch=1;
                
                perturbation_samples = atoi(argv[d+1]);
                perturbation_size    = atof(argv[d+2]);
                
                // WE CHECK HERE THAT perturbation_samples IS GREATER THAN 0 AND THAT perturbation_size IS AT LEAST 0
                if(0 < perturbation_samples && 0 <= perturbation_size)
                    d+=2;
                else
                {
                    help_message();
                    std::cout << "-g option indicated; must be followed by two numbers, samples and perturbation.\n";
                    exit(0);
                }
            }
        }
        
        else if(strcmp(argv[d],"-c") ==0)                  // CLUSTER ANALYSIS; TAKES OPTIONAL ARGUMENT
        {
            c_switch=1;

            if(argc > d+1 && argv[d+1][0]!='-')
            {
                clustering_default = atoi(argv[d+1]);
                if(0 <= clustering_default)
                    d++;
                else
                {
                    help_message();
                    std::cout << "-c option indicated; may be followed by optional index for clustering.\n";
                    exit(0);
                }
                clustering_default_switch = 1;
            }
            else
            {
                clustering_default        = 0;
                clustering_default_switch = 0;
            }
        }
        
        else if(strcmp(argv[d],"-r") ==0)                  // RADIUS RESCALING FOR PARTICLE SIZES FOR EPS DRAWINGS
        {
            r_switch=1;
            if(argc > d+1 && argv[d+1][0]!='-')
                radius_scaling = atof(argv[d+1]);
            d++;
        }
 
        else if(strcmp(argv[d],"-t") ==0)                  // SPECIFY NUMBER OF THREADS FOR MULTITHREADED VERSION
        {
            if(argc > d+1 && argv[d+1][0]!='-')
                threads = atoi(argv[d+1]);
            d++;
        }
 
        // BY DEFAULT, INPUT FILE IS USED AS A BASENAME FOR OUTPUT FILES.  NEW
        // BASENAME OR OUTPUT DIRECTORIES CAN BE SPECIFIED.
        else if(strcmp(argv[d],"-od")==0)                       // SPECIFY OUTPUT DIRECTORY
        {
            if(argc > d+1 && argv[d+1][0]!='-')
            {
                path = argv[d+1];                               // WE HAVE CHANGED THE PATH
                if(path.back()!='\\' && path.back()!='/')
                    path.append("/");
                d++;
                
                // IF SPECIFIED OUTPUT DIRECTORY DOES NOT EXIST, CREATE IT
                struct stat st;
                if (stat(path.c_str(), &st) == -1)
                    mkdir(path.c_str(), 0744);
            }
            else
            {
                help_message();
                std::cout << "-od option indicated but no output directory specified.\n";
                exit(0);
            }
        }
        
        else if(strcmp(argv[d],"-o")==0)                        // SPECIFY OUTPUT FILE NAME
        {
            if(argc > d+1 && argv[d+1][0]!='-')
            {
                file = argv[d+1];                               // WE HAVE CHANGED THE FILE NAME
                d++;
            }
            else
            {
                help_message();
                std::cout << "-o option indicated but no output file specified.\n\n";
                exit(0);
            }
        }
        
        else
        {
            help_message();
            std::cout << "Unidentified option \'" << argv[d] << "\' included.\n\n";
            exit(0);
        }
    }
    
    if(c_switch && e_switch) particle_coloring_scheme=5;                             // IF SYSTEM WILL BE DRAWN

    // ALSO IF COLORING SCHEME 3 BUT NO FILTER...
    if(((e_switch && particle_coloring_scheme==5) || c_switch) && !f_switch && !p_switch)
    {
        std::cout << "-c option requires filter file for cluster analysis\n\n";
        exit(0);
    }
    
    if(p_switch && f_switch)
    {
        std::cout << "-p option not compatible with other options\n\n";
        exit(0);
    }
    
    filename_output = path + file;
}





