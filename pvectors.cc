////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: pvectors.cc

////    void calc_neighbors(voro::container_2d& con)
////    void calc_all_pvectors()
////    void calc_distribution(pFilter &pfilter)
////    int print_pvectors(std::string filename)


#include <vector>
#include <cstring>
#include <fstream>
#include <iostream>

#include "filters.hh"
#include "variables.hh"

using namespace voro;


////////////////////////////////////////////////////
////
////    CALCULATES THE NUMBER OF SIDES AND THE LIST
////    OF NEIGHBORS OF EACH PARTICLE. NECESSARY
////    FOR CALCULATING P-VECTORS AND FOR SOME DRAWING
////
////////////////////////////////////////////////////
void calc_neighbors(voro::container_2d& con)
{
#pragma omp parallel for num_threads(threads) schedule(dynamic)
    for(voro::container_2d::iterator cli=con.begin();cli<con.end();cli++)
    {
        voro::voronoicell_neighbor_2d c(con);
        if (con.compute_cell(c,cli))
        {
            int ijk=cli->ijk,q=cli->q;
            int pid = con.id[ijk][q];
            c.neighbors(neighbors_list[pid]);
            neighbors_list_c[pid] = neighbors_list[pid].size();
        }
        else
        {
            std::cout << "A Voronoi cell failed to compute\n";
            exit(0);
        }
    }
}


////////////////////////////////////////////////////
////
////    CALCULATES THE P-VECTOR FOR EACH PARTICLE.
////    REQUIRES PREVIOUSLY CALLING calc_neighbors()
////
////////////////////////////////////////////////////
void calc_all_pvectors()
{
#pragma omp parallel for num_threads(threads) schedule(dynamic)
    for(int c=0; c<number_of_particles; c++)
        all_pvectors[c] = calc_pvector(c);
}


////////////////////////////////////////////////////
////
////    SHORTENS EACH P-VECTOR TO NOT INCLUDE THE
////    SYMMETRY ORDER OR CHIRALITY
////
////////////////////////////////////////////////////
void shorten_pvectors()
{
#pragma omp parallel for num_threads(threads) schedule(dynamic)
    for(int c=0; c<number_of_particles; c++)
        all_pvectors[c].resize(all_pvectors[c][0]+1);
}


////////////////////////////////////////////////////
////
////    CALCULATES THE P-VECTOR FOR A SINGLE PARTICLE.
////    USES INFORMATION COMPUTED IN calc_neighbors(),
////    WHICH COMPUTES THE NUMBER OF NEIGHBORS OF EACH
////    PARTICLE, AND A LIST OF THOSE NEIGHBORS.
////
////////////////////////////////////////////////////
std::vector<int> calc_pvector(int pid)
{
    ////////////////////////////////////////////////////////////////
    // BUILD THE CANONICAL CODE
    ////////////////////////////////////////////////////////////////    
    double xdim = hi_bound[0]-origin[0];
    double ydim = hi_bound[1]-origin[1];

    // NUMBER OF NEIGHBORS
    unsigned int nneighbors = neighbors_list[pid].size();
    
    // LIST OF ANGLES FROM 0 OF NEIGHBORS, ALONG WITH NEIGHBOR ID
    std::vector< std::pair <double,int> > prepvect;
    
    // WILL ARRANGE VORONOI NEIGHBORS IN COUNTERCLOCKWISE ORDER
    for(unsigned int q=0; q<nneighbors; q++)
    {
        double dx = particle_x_coordinates[neighbors_list[pid][q]] - particle_x_coordinates[pid];
        double dy = particle_y_coordinates[neighbors_list[pid][q]] - particle_y_coordinates[pid];
        if(dx >  xdim/2) dx -= xdim;
        if(dx < -xdim/2) dx += xdim;
        if(dy >  ydim/2) dy -= ydim;
        if(dy < -ydim/2) dy += ydim;
        double theta1 = atan(dy/dx) + 3.14159265359/2.;
        if(dx<0) theta1 += 3.14159265359;
        
        prepvect.push_back(std::make_pair(theta1,neighbors_list_c[neighbors_list[pid][q]]));
        std::sort(prepvect.begin(), prepvect.end());
    }
    
    std::vector< std::vector <int> > numbers1;
    std::vector< std::vector <int> > numbers2;
    
    for(unsigned int c=0; c<nneighbors; c++)
    {
        std::vector<int> v;
        for(unsigned int d=0; d<nneighbors; d++)
            v.push_back(prepvect[(c+d)%nneighbors].second);
        numbers1.push_back(v);
    }
    for(unsigned int c=0; c<nneighbors; c++)
    {
        std::vector<int> v;
        for(unsigned int d=0; d<nneighbors; d++)
            v.push_back(prepvect[(c-d+nneighbors)%nneighbors].second);
        numbers2.push_back(v);
    }
    
    std::sort(numbers1.begin(), numbers1.end());
    std::sort(numbers2.begin(), numbers2.end());

    int chirality = 0;
    if     (numbers1[0] < numbers2[0]) chirality =  1;
    else if(numbers1[0] > numbers2[0]) chirality = -1;

    int symmetry_order = 1;
    for(unsigned int c=0; c<nneighbors; c++)
        if(numbers1[c] == numbers1[0])
            symmetry_order = c+1;
    
    if(chirality==0) symmetry_order *= 2;
    if(chirality==-1)
    {
        numbers2[0].insert(numbers2[0].begin(), nneighbors);
        numbers2[0].push_back(symmetry_order);
        numbers2[0].push_back(chirality);
    }
    else
    {
        numbers1[0].insert(numbers1[0].begin(), nneighbors);
        numbers1[0].push_back(symmetry_order);
        numbers1[0].push_back(chirality);
    }
    
    if(chirality==-1) return numbers2[0];
    else              return numbers1[0];
}


////////////////////////////////////////////////////
////
////   ADDS COMPUTED PVECTORS TO FILTER MAP
////
////////////////////////////////////////////////////

void calc_distribution(pFilter &pfilter)
{
    for(int c=0; c<number_of_particles; c++)
        pfilter.increment_or_add(all_pvectors[c],1);
    pfilter.relabel_data_types();
}


////////////////////////////////////////////////////
////
////   ADDS COMPUTED PVECTORS TO FILTER MAP
////
////////////////////////////////////////////////////

void calc_distribution_chiral(pFilter &pfilter)
{
    for(int c=0; c<number_of_particles; c++)
        pfilter.increment_chiral(all_pvectors[c]);
    pfilter.relabel_data_types();
}


int print_pvectors(std::string filename)
{
    std::string pvectors_name(filename);
    pvectors_name.append(".pvectors");
    std::ofstream pvector_file(pvectors_name.c_str(), std::ofstream::out);
    
    for(int c=0; c<number_of_particles; c++)
    {
        unsigned int psize = all_pvectors[c].size()-3;
        
        int p2pvector[50];                          // HERE WE STORE THE NUMBER OF NEIGHBORS WITH EACH NUMBER OF SIDES
        for(int d=0; d<50; d++) p2pvector[d]=0;     //
        int maxp = 0;                               // STORE THE MAX NUMBER OF EDGES AMONG NEIGHBORS
        
        for(unsigned int d=1; d<=psize; d++)
        {
            p2pvector[all_pvectors[c][d]]++;
            if(all_pvectors[c][d]>maxp) maxp = all_pvectors[c][d];
        }
        
        pvector_file << all_pvectors[c][0] << '\t';
        pvector_file << "(";
        for(int d=3; d<maxp; d++)
            pvector_file << p2pvector[d] << ",";
        pvector_file << p2pvector[maxp] << ")\t";
        
        pvector_file << "(";
        for(unsigned int d=0; d<psize; d++)
            pvector_file << all_pvectors[c][d] << ",";
        pvector_file << all_pvectors[c][psize] << ")\t";
        pvector_file << all_pvectors[c][psize+1] << '\t';
        pvector_file << all_pvectors[c][psize+2] << '\n';
    }
    
    return 0;
}




