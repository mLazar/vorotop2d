////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: vorotop.cc


#include <vector>
#include <cstring>
#include <fstream>
#include <iostream>
#include <algorithm>

#include "filters.hh"
#include "variables.hh"
#include "functions.hh"


int main(int argc, char *argv[])
{
    ////////////////////////////////////////////////////
    ////
    ////   PARSE COMMAND-LINE OPTIONS.  DETERMINE
    ////   FILE NAMES AND DIRECTORIES TO USE, AND
    ////   WHICH ANALYSES TO PERFORM.
    ////
    ////////////////////////////////////////////////////
    
    parse_arguments(argc, argv);
    
    
    ////////////////////////////////////////////////////
    ////
    ////   OPEN INPUT FILE
    ////
    ////////////////////////////////////////////////////
    
    data_file.open(name_of_data_file, std::ifstream::in);
    if(!data_file.is_open())
    {
        help_message();
        std::cerr << "Unable to open input file " << name_of_data_file << "\n\n";
        exit(-1);
    }
    
    ////////////////////////////////////////////////////
    ////
    ////   VARIABLES AND HELP MESSAGES IF REQUESTED
    ////
    ////////////////////////////////////////////////////
    
    if(d_switch==0 && g_switch==0 && e_switch==0 && c_switch==0 &&
       f_switch==0 && p_switch==0 && u_switch==0 && v_switch==0)
    {
        help_message();
        exit(-1);
    }
    

    ////////////////////////////////////////////////////
    ////
    ////   PARSE DATA FILE TO SYSTEM DIMENSIONS
    ////
    ////////////////////////////////////////////////////
    
    parse_header(data_file);
    

    ////////////////////////////////////////////////////
    ////
    ////   ALLOCATE MEMORY FOR DATA
    ////
    ////////////////////////////////////////////////////
    
    particle_x_coordinates.resize(number_of_particles); // MEMORY TO STORE PARTICLE COORDINATES
    particle_y_coordinates.resize(number_of_particles);

    all_pvectors.resize    (number_of_particles);       // MEMORY FOR PVECTORS
    neighbors_list.resize  (number_of_particles);       // MEMORY FOR STORING LISTS OF NEIGHBORS
    neighbors_list_c.resize(number_of_particles);       // MEMORY FOR STORING NUMBER OF NEIGHBORS

    vt_structure_types.resize(number_of_particles);     // MEMORY FOR STRUCTURE TYPES
    cluster_index.resize   (number_of_particles);       // MEMORY FOR CLUSTER ANALYSIS
    cluster_sizes.resize   (number_of_particles);
    ring_index.resize      (number_of_particles);       // MEMORY FOR COLORING BY DISTANCE FROM CENTER


    ////////////////////////////////////////////////////
    ////
    ////   CREATE VORO++ CONTAINER; THEN ADD PARTICLES
    ////
    ////////////////////////////////////////////////////
    
    voro::container_2d con (origin[0],hi_bound[0],origin[1],hi_bound[1],n_x,n_y,true,true,8,threads);
    import_data (data_file, con);


    ////////////////////////////////////////////////////
    ////
    ////   LOAD FILTER IF REQUESTED
    ////
    ////////////////////////////////////////////////////

    pFilter pfilter;
    if(f_switch) pfilter.loadpFilter(filename_pfilter);


    ////////////////////////////////////////////////////
    ////
    ////   COMPUTE P-VECTORS
    ////
    ////////////////////////////////////////////////////

    calc_neighbors(con);
    calc_all_pvectors();
    if(f_switch)
    {
        shorten_pvectors();
        calc_pstructure_types(pfilter);
    }


    ////////////////////////////////////////////////////
    ////
    ////   PROCESS DATA ACCORDING TO INPUT OPTIONS
    ////
    ////////////////////////////////////////////////////
    
    if(p_switch)
        print_pvectors(filename_output);

    else if(d_switch)
    {
        calc_distribution          (pfilter);
        pfilter.print_distribution (filename_output);
        calc_distribution_chiral   (pfilter);
        pfilter.print_distribution_chiral(filename_output);
    }
    
    else if(g_switch)   // WE MIGHT NEED TO CLEAR THE DISTRIBUTION BEFORE THE PERTURBATIONS
    {
        calc_gaussian_distribution (con,pfilter);
        pfilter.print_distribution(filename_output);
    }

    else if(u_switch || v_switch)  pair_correlation_analysis ();

    else if(e_switch)
    {
        // COLORING SCHEMES 0,1,2,3 REQUIRE NO ANALYSIS;
        // COLORING SCHEMES 4 AND 5 REQUIRE ANALYSIS
        // BEFORE DRAWING THE EPS.
        if(particle_coloring_scheme == 4) ring_coloring();
        if(particle_coloring_scheme == 5) cluster_analysis();
        if(particle_coloring_scheme == 6) defect_cluster_analysis();
        output_eps(con,filename_output);
    }

    else if(c_switch)              cluster_analysis ();
    else if(f_switch)              output_system (filename_output,pfilter);
    
    return 0;
}




