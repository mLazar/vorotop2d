////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: functions.hh


#ifndef __FUNCTIONS_H_INCLUDED__
#define __FUNCTIONS_H_INCLUDED__  

void help_message       (void);
void parse_arguments    (int argc, char *argv[]);

int  parse_header       (std::ifstream&);
void import_data        (std::ifstream&, voro::container_2d &con);

void calc_neighbors(voro::container_2d& con);
void calc_all_pvectors();
void shorten_pvectors();
void calc_pstructure_types(pFilter &pfilter);
void calc_distribution (pFilter& filter);
void calc_distribution_chiral (pFilter& filter);
void cluster_analysis  (void);
void defect_cluster_analysis(void);
void pair_correlation_analysis(void);

int  percolation_analysis(double p);
void calc_gaussian_distribution (voro::container_2d& con, pFilter& filter);

int  print_pvectors   (std::string filename);
void output_system    (std::string filename, pFilter &filter);
void output_eps       (voro::container_2d& con, std::string filename);
void ring_coloring    ();

#endif




