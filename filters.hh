////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: filters.hh


#include "voro++.hh"
#include <map>
#include <algorithm>


#ifndef __FILTERS_H_INCLUDED__
#define __FILTERS_H_INCLUDED__   

using pVector        = std::vector<int>;

struct pFilterEntry
{
    int type;
    int count;
    int source; // 0 filter file, 1 data
    int chirals[3]; // counts of chiral versions

    pFilterEntry(int t)               : type(t), count(0), source(0) { chirals[0]=chirals[1]=chirals[2]=0; };
    pFilterEntry(int t, int c, int s) : type(t), count(c), source(s) { chirals[0]=chirals[1]=chirals[2]=0; };
    pFilterEntry(int t, int c, int s, int chir) : type(t), count(c), source(s) { chirals[0]=chirals[1]=chirals[2]=0; chirals[chir+1]=1; };
};


class pFilter {
    std::map<pVector,pFilterEntry> entries;
    std::vector<std::string> structure_types;

public:
    int max_file_filter_type;
    int max_filter_type;
    int file_filter_types;
    
    pFilter() : max_file_filter_type(0), max_filter_type(0) {}
    
    void loadpFilter(std::string);
    void print_distribution(std::string);
    void print_distribution_chiral(std::string);
    void relabel_data_types();
    
    int  pvector_type    (pVector);
    void increment_or_add(pVector, int n);
    void increment_chiral(pVector);
};

pVector calc_pvector (int pid);

#endif







