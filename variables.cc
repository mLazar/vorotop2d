////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: variables.cc


#include <vector>
#include <fstream>
#include <sstream>


int    number_of_particles;
double origin[2];
double hi_bound[2];
double supercell_edges[2][2];
bool scaled_coordinates;

int header_lines;

std::ifstream data_file;

std::vector<std::string> attribute_labels;
std::vector<std::vector <int> > all_pvectors;
std::vector<std::vector <int> > neighbors_list;
std::vector<int> vt_structure_types;

std::vector<int> neighbors_list_c;
std::vector<int> cluster_index;
std::vector<int> cluster_sizes;
std::vector<int> ring_index;
std::vector<double> particle_x_coordinates;
std::vector<double> particle_y_coordinates;

std::string name_of_data_file;
std::string filename_output;
std::string filename_pfilter;

int  xindex;
int  n_x,n_y;

bool e_switch;    // OUTPUT EPS FILE
bool f_switch;    // LOAD FILTER FILE
bool p_switch;    // LOAD pFILTER FILE
bool w_switch;    // PRINT P-VECTORS
bool d_switch;    // COMPUTE DISTRIBUTION OF P-VECTORS
bool g_switch;    // COMPUTE DISTRIBUTION BASED ON PERTURBATIONS OF SYSTEM
bool c_switch;    // PERFORM CLUSTER ANALYSIS
bool u_switch;    // FOR COMPUTING UNNORMALIZED VORONOI PAIR CORRELATION FUNCTION
bool v_switch;    // FOR COMPUTING VORONOI PAIR CORRELATION FUNCTION
bool r_switch;    // CHANGE PARTICLE RADIUS FOR EPS DRAWINGS
bool n_switch;    // DO NOT DRAW VORONOI CELLS; ONLY DRAW PARTICLES

int     perturbation_samples;
double  perturbation_size;
double  radius_scaling;

int     clustering_default;
int     clustering_default_switch;

int     threads;
int     max_radius;
int     particle_coloring_scheme;

int     particles_in_eps;

