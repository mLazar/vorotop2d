////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: variables.hh


#ifndef __VOROTOP_H_INCLUDED__
#define __VOROTOP_H_INCLUDED__   

extern int number_of_particles;
extern int xindex;

extern std::ifstream data_file;

extern double origin[2];
extern double hi_bound[2];
extern double supercell_edges[2][2];

extern bool scaled_coordinates;

extern int header_lines;

extern std::string name_of_data_file;
extern std::string filename_output;
extern std::string filename_pfilter;

extern std::vector<std::string> attribute_labels;

extern std::vector<std::vector <int> > all_pvectors;

extern std::vector<std::vector <int> > neighbors_list;
extern std::vector<int> vt_structure_types;

extern std::vector <int> neighbors_list_c;
extern std::vector <int> cluster_index;
extern std::vector <int> cluster_sizes;
extern std::vector <int> ring_index;
extern std::vector <double> particle_x_coordinates;
extern std::vector <double> particle_y_coordinates;
extern std::vector <double> areas;
extern std::vector <double> std_angle;

extern int n_x,n_y;

extern bool e_switch;    // OUTPUT EPS FILE
extern bool f_switch;    // LOAD FILTER FILE
extern bool p_switch;    // COMPUTE P-VECTORS
extern bool d_switch;    // COMPUTE DISTRIBUTION OF P-VECTORS
extern bool g_switch;    // COMPUTE DISTRIBUTION BASED ON PERTURBATIONS OF SYSTEM
extern bool c_switch;    // PERFORM CLUSTER ANALYSIS
extern bool u_switch;    // COMPUTE UNNORMALIZED VORONOI PAIR CORRELATION FUNCTION
extern bool v_switch;    // COMPUTE VORONOI PAIR CORRELATION FUNCTION
extern bool r_switch;    // CHANGE PARTICLE RADIUS FOR EPS DRAWINGS
extern bool n_switch;    // DO NOT DRAW VORONOI CELLS; ONLY DRAW PARTICLES

extern int    perturbation_samples;
extern double perturbation_size;
extern double radius_scaling;

extern int    clustering_default;
extern int    clustering_default_switch;

extern int    threads;
extern int    max_radius;
extern int    particle_coloring_scheme;

extern int     particles_in_eps;

#endif



