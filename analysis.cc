////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: analysis.cc


#include <set>
#include <queue>
#include <random>
#include <iostream>

#include "filters.hh"
#include "variables.hh"
#include "functions.hh"
#include <bits/stdc++.h>


bool vector_size(const std::vector<int>& a,const std::vector<int>& b)
{
    return (a.size() > b.size());
}



void   pair_correlation_analysis(void)
{
    // HOW FAR AWAY FROM CENTRAL PARTICLE; DEFAULT
    // SETTING IS 20; MAX VALUE IS 127.
    
    // FOR TALLYING DATA OVER ALL PARTICLES
    std::vector<unsigned long long int> neighbor_counts   (max_radius+1);
    std::vector<unsigned long long int> neighbor_counts_sq(max_radius+1);
    
    // FOR NORMALIZING COUNTS
    std::vector<double>                 normalization     (max_radius+1);
    
    if(u_switch==1)
        for(int k=1; k<=max_radius; k++)
            normalization[k]=1.;
    
    else
    {
        //        double c0=17.169-1.36456;
        double c1=12.5838;
        double c2=-0.488085;
        double c3=-24.8677;
        
        for(int k=1; k<=max_radius; k++)
            normalization[k] = 6.+c1*(double(k)-1.) + c2*(pow(double(k),0.5)-1.) + c3*(pow(double(k),0.25)-1.);
    }
    
    // FOR EACH PARTICLE, COMPUTE GROWING CLUSTER AROUND IT
    // COUNTING ITS NUMBER OF NEIGHBORS IN EACH "RING"
    
#pragma omp parallel for num_threads(threads) schedule(dynamic)
    for(int p=0; p<number_of_particles; p++)
    {
        // cluster STORES ALL PARTICLE ID'S IN THE GROWING CLUSTER
        // visited ASSOCIATES TO EACH PARTICLE ID THE SHELL IT IS IN OR -1
        std::vector<unsigned int> cluster(number_of_particles, 0);
        std::vector<signed char>  visited(number_of_particles,-1);
        
        // kneighbors RECORDS NUMBER OF NEIGHBORS IN EACH RING.
        std::vector<int> kneighbors(max_radius+1,0);
        
        // visited TRACKS WHICH PARTICLES HAVE BEEN INCLUDED.
        // INITIALLIZED WITH -1, AND THEN UPDATED TO DISTANCE k.
        int counter=0;
        visited[p]=0;
        cluster[counter++]=p;
        kneighbors[0]=1;
        
        int begin= 0;
        int end  = 1;
        
        // BUILD EACH RING, ADDING UNVISITED NEIGHBORS OF PRIOR RING
        for(int k=1; k<=max_radius; k++)
        {
            // ITERATE OVER ALL PARTICLES IN PRIOR RING
            for(int c=begin; c<end; c++)
            {
                // ID OF CURRENT PARTICLE
                int tempc      = cluster[c];
                int nneighbors = neighbors_list_c[tempc];
                
                // ITERATE OVER ALL ITS NEIGHBORS
                for(int d=0; d<nneighbors; d++)
                {
                    if(visited[neighbors_list[tempc][d]] == -1)
                    {
                        visited[neighbors_list[tempc][d]]=k;
                        cluster[counter++]=neighbors_list[tempc][d];
                        kneighbors[k]++;
                    }
                }
                
            }
            begin = end;
            end   = end+kneighbors[k];
            
#pragma omp atomic
            neighbor_counts[k]    += kneighbors[k];
#pragma omp atomic
            neighbor_counts_sq[k] += kneighbors[k]*kneighbors[k];
        }
    }
    
    
    ////////////////////////////////////////////////////
    ////
    ////   OUTPUT BASIC ANALYSIS OF VPCF DATA
    ////
    ////////////////////////////////////////////////////
    
    double total=1;         // WILL BE USED TO COMPUTE WHAT FRACTION OF SYSTEM IS COVERED
    // BY CLUSTERS OF RADIUS k.  THIS NUMBER SHOULD NOT BE CLOSE
    // TO 1, OR ELSE THIS INDICATES "WRAP-AROUND" EFFECTS.
    
    for(int k=1; k<=max_radius; k++)
    {
        double avg =      (double(neighbor_counts[k])   /double(number_of_particles))/normalization[k];
        double variance = (double(neighbor_counts_sq[k])/double(number_of_particles) - avg*avg*normalization[k]*normalization[k])/normalization[k]/normalization[k];
        double std = sqrt(variance);
        
        total += double(neighbor_counts[k]) / double(number_of_particles);
        std::cout << k << '\t' << avg << '\t' << variance << '\t' << std << '\t' << total/double(number_of_particles) << '\n';
    }
    std::cout << '\n';
}


////////////////////////////////////////////////////
////
////   DETERMINE "CLUSTERS" OF NON-CRYSTALLINE PARTICLES;
////   BY DEFAULT, ONLY TYPES NOT APPEARING IN A FILTER
////   ARE CLASSIFIED AS DEFECTS AND ARE CLUSTERED.  IF
////   AN OPTIONAL STRUCTURE TYPE IS PROVIDED AT THE COMMAND
////   LINE THEN ALL PARTICLES WITH OTHER STRUCTURE TYPES ARE
////   CONSIDERED DEFECTS FOR THIS CLUSTERING PURPOSE.
////
////////////////////////////////////////////////////

void   cluster_analysis(void)
{
    int total_defect_particles  = 0;
    int total_crystal_particles = 0;
    
    std::vector <std::vector <int>> clusters_good;
    std::vector <std::vector <int>> clusters_bad;
    
    
    if(clustering_default_switch == 0)
        for(int c=0; c<number_of_particles; c++) cluster_index[c] = vt_structure_types[c];
    
    else
    {
        for(int c=0; c<number_of_particles; c++)
        {
            if(vt_structure_types[c]==clustering_default) cluster_index[c] = 1;
            else                                          cluster_index[c] = 0;
        }
    }
    
    // BUILD DEFECT CLUSTERS IN O(N) TIME; FOR USE
    // IN SYSTEMS THAT ARE PRIMARILY CRYSTALLINE.
    std::vector <int> visited(number_of_particles,0);
    for(int d=0; d<number_of_particles; d++)
    {
        if(cluster_index[d]==0) total_defect_particles++;
        else                    visited[d]=1;
    }
    
    for(int d=0; d<number_of_particles; d++)
    {
        if(visited[d]==0)
        {
            std::vector<int> cluster;
            std::queue<int> q;
            q.push(d);
            visited[d]=1;
            
            while(!q.empty())
            {
                int w = q.front();
                cluster.push_back(w);
                q.pop();
                
                for(int e=0; e<neighbors_list_c[w]; e++)
                {
                    if(visited[neighbors_list[w][e]]==0)
                    {
                        visited[neighbors_list[w][e]]=1;
                        q.push(neighbors_list[w][e]);
                    }
                }
            }
            clusters_bad.push_back(cluster);
        }
    }
    sort(clusters_bad.begin(),clusters_bad.end(),vector_size);
    
    
    // BUILD CRYSTALLINE CLUSTERS IN O(N) TIME; FOR USE
    // IN SYSTEMS THAT ARE PRIMARILY DISORDERED.
    std::fill(visited.begin(),visited.end(), 0);
    for(int d=0; d<number_of_particles; d++)
    {
        if(cluster_index[d]>0)  total_crystal_particles++;
        else                    visited[d]=1;
    }
    
    for(int d=0; d<number_of_particles; d++)
    {
        if(visited[d]==0)
        {
            std::vector<int> cluster;
            std::queue<int> q;
            q.push(d);
            visited[d]=1;
            
            while(!q.empty())
            {
                int w = q.front();
                cluster.push_back(w);
                q.pop();
                
                for(int e=0; e<neighbors_list_c[w]; e++)
                {
                    if(visited[neighbors_list[w][e]]==0)
                    {
                        visited[neighbors_list[w][e]]=1;
                        q.push(neighbors_list[w][e]);
                    }
                }
            }
            clusters_good.push_back(cluster);
        }
    }
    sort(clusters_good.begin(),clusters_good.end(),vector_size);
    
    
    ////////////////////////////////////////////////////
    // ASSIGN AND RECORD FOR EACH PARTICLE A CLUSTER ID, STORED IN
    // cluster_index[]. NEGATIVE INDICES INDICATE DEFECT CLUSTERS;
    // POSITIVE INDICES INDICATE CRYSTALLINE CLUSTERS. ALSO RECORD
    // FOR EACH PARTICLE THE NUMBER OF PARTICLES IN ITS CLUSTER,
    // STORED IN cluster_sizes[]
    for(unsigned int c=0; c<clusters_bad.size(); c++)
    {
        unsigned int size = clusters_bad[c].size();
        for(unsigned int d=0; d<size; d++)
        {
            cluster_index[clusters_bad[c][d]]=-c-1;
            cluster_sizes[clusters_bad[c][d]]=size;
        }
    }
    
    for(unsigned int c=0; c<clusters_good.size(); c++)
    {
        unsigned int size = clusters_good[c].size();
        for(unsigned int d=0; d<size; d++)
        {
            cluster_index[clusters_good[c][d]]=c+1;
            cluster_sizes[clusters_good[c][d]]=size;
        }
    }
    
    
    ////////////////////////////////////////////////////
    ////
    ////   OUTPUT BASIC ANALYSIS OF CLUSTERING DATA TO SCREEN
    ////
    ////////////////////////////////////////////////////
    
    unsigned int max_cluster_size = 100;
    std::vector<int> c_sizes(max_cluster_size,0);
    double sum_of_squares = 0;
    
    for(unsigned int c=0; c<clusters_bad.size(); c++)
    {
        if(clusters_bad[c].size()<max_cluster_size)
            c_sizes[clusters_bad[c].size()]++;
        sum_of_squares += clusters_bad[c].size()*clusters_bad[c].size();
    }
    
    std::cout << name_of_data_file      << '\t';
    std::cout << number_of_particles    << '\t';
    std::cout << total_defect_particles << '\t';
    std::cout << clusters_bad.size()    << '\t';
    std::cout << clusters_good.size()   << '\t';
    
    if(!clusters_bad.empty())
    {
        double average = double(total_defect_particles)/double(clusters_bad.size());
        double stdev   = sqrt(double(sum_of_squares)/double(clusters_bad.size()) - average*average);
        
        std::cout << (clusters_bad. back()).size() << '\t';  // SIZE OF SMALLEST DEFECT CLUSTER
        std::cout << (clusters_bad.front()).size() << '\t';  // SIZE OF LARGEST DEFECT CLUSTER
        std::cout << average                       << '\t';  // AVERAGE SIZE OF CLUSTER
        std::cout << stdev                         << '\t';  // STANDARD DEVIATION OF CLUSTER SIZE
    }
    else
        std::cout << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t';
    
    for(unsigned int c=1; c<max_cluster_size; c++)       // NUMBER OF CLUSTERS WITH 1, 2, 3, ..., max_cluster_size-1 PARTICLES
        std::cout << c_sizes[c] << '\t';
    
    std::cout << '\n';
}



////////////////////////////////////////////////////
////
////   DETERMINE "CLUSTERS" OF NON-CRYSTALLINE PARTICLES;
////   BY DEFAULT, ONLY TYPES NOT APPEARING IN A FILTER
////   ARE CLASSIFIED AS DEFECTS AND ARE CLUSTERED.  IF
////   AN OPTIONAL STRUCTURE TYPE IS PROVIDED AT THE COMMAND
////   LINE THEN ALL PARTICLES WITH OTHER STRUCTURE TYPES ARE
////   CONSIDERED DEFECTS FOR THIS CLUSTERING PURPOSE.
////
////////////////////////////////////////////////////

void   defect_cluster_analysis(void)
{
    int total_defect_particles  = 0;
    std::vector <std::vector <int>> clusters_bad;
    
    int typeA_vacancy_count=0;
    int typeB_vacancy_count=0;
    int typeC_vacancy_count=0;
    int dislocation_count  =0;
    int grain_boundary_count=0;
    
    // IF USING clustering_default_switch FEATURE, THEN WE WANT TO
    // GROUP PARTICULAR KINDS OF DEFECT PARTICLES TOGETHER.  FOR EXAMPLE
    // SIX VACANCY DEFECTS CAN BE COMBINED AS A VACANCY, ETC.
    if(clustering_default_switch == 0)
        for(int c=0; c<number_of_particles; c++)
        {
            cluster_index[c] = vt_structure_types[c];
            if(cluster_index[c]==1) cluster_index[c]=0;
        }
    
    else
    {
        for(int c=0; c<number_of_particles; c++)
        {
            if(vt_structure_types[c]==clustering_default) cluster_index[c] = 1;
            else                                          cluster_index[c] = 0;
        }
    }
    
    // BUILD DEFECT CLUSTERS IN O(N) TIME; FOR USE
    // IN SYSTEMS THAT ARE PRIMARILY CRYSTALLINE.
    std::vector <int> visited(number_of_particles,0);
    for(int d=0; d<number_of_particles; d++)
    {
        if(cluster_index[d]==2 || cluster_index[d]==3 || cluster_index[d]==4) total_defect_particles++;
        //if(cluster_index[d]==0) total_defect_particles++;
        else                    visited[d]=1;
    }
    
    for(int d=0; d<number_of_particles; d++)
    {
        if(visited[d]==0)
        {
            std::vector<int> cluster;
            std::queue<int> q;
            q.push(d);
            visited[d]=1;
            
            while(!q.empty())
            {
                int w = q.front();
                cluster.push_back(w);
                q.pop();
                
                for(int e=0; e<neighbors_list_c[w]; e++)
                {
                    if(visited[neighbors_list[w][e]]==0)
                    {
                        visited[neighbors_list[w][e]]=1;
                        q.push(neighbors_list[w][e]);
                    }
                }
            }
            clusters_bad.push_back(cluster);
        }
    }
    sort(clusters_bad.begin(),clusters_bad.end(),vector_size);
    
    std::cout << "We have now " << clusters_bad.size() << " defect clusters\n";
    
    
    for(unsigned int c=0; c<clusters_bad.size(); c++)
    {
        bool all_type_3=1;
        bool all_type_4=1;
        bool no_type_2 =1;
        bool no_type_4 =1;
        
        unsigned int size = clusters_bad[c].size();
        std::cout << "Cluster with " << size << " particles " << '\n';
        for(unsigned int d=0; d<size; d++)
        {
            if(vt_structure_types[clusters_bad[c][d]]==2) no_type_2 =0;
            if(vt_structure_types[clusters_bad[c][d]]==4) no_type_4 =0;
            if(vt_structure_types[clusters_bad[c][d]]!=3) all_type_3=0;
            if(vt_structure_types[clusters_bad[c][d]]!=4) all_type_4=0;
            std::cout << clusters_bad[c][d] << '\t' << vt_structure_types[clusters_bad[c][d]] << '\n';
            
            //            cluster_index[clusters_bad[c][d]]=-c-1;
            //            cluster_sizes[clusters_bad[c][d]]=size;
        }
        if(size==6 && all_type_4==1) typeA_vacancy_count++;
        if(size==6 && no_type_2 ==1)
        {
            for(unsigned int d=0; d<size; d++)
                vt_structure_types[clusters_bad[c][d]]=4;
            typeB_vacancy_count++;
        }
        if(size==3 && all_type_4==1) typeC_vacancy_count++;
        if(size==2 && all_type_3 ==1) dislocation_count++;
        
        if(size>2  && no_type_4 ==1)
        {
            for(unsigned int d=0; d<size; d++)
                vt_structure_types[clusters_bad[c][d]]=2;
            grain_boundary_count++;
        }
        std::cout << '\n';
    }
    
    ////////////////////////////////////////////////////
    // ASSIGN AND RECORD FOR EACH PARTICLE A CLUSTER ID, STORED IN
    // cluster_index[]. NEGATIVE INDICES INDICATE DEFECT CLUSTERS;
    // POSITIVE INDICES INDICATE CRYSTALLINE CLUSTERS. ALSO RECORD
    // FOR EACH PARTICLE THE NUMBER OF PARTICLES IN ITS CLUSTER,
    // STORED IN cluster_sizes[]
    for(unsigned int c=0; c<clusters_bad.size(); c++)
    {
        unsigned int size = clusters_bad[c].size();
        for(unsigned int d=0; d<size; d++)
        {
            cluster_index[clusters_bad[c][d]]=-c-1;
            cluster_sizes[clusters_bad[c][d]]=size;
        }
    }
    
    
    ////////////////////////////////////////////////////
    ////
    ////   OUTPUT BASIC ANALYSIS OF CLUSTERING DATA TO SCREEN
    ////
    ////////////////////////////////////////////////////
    
    unsigned int max_cluster_size = 100;
    std::vector<int> c_sizes(max_cluster_size,0);
    double sum_of_squares = 0;
    
    for(unsigned int c=0; c<clusters_bad.size(); c++)
    {
        if(clusters_bad[c].size()<max_cluster_size)
            c_sizes[clusters_bad[c].size()]++;
        sum_of_squares += clusters_bad[c].size()*clusters_bad[c].size();
    }
    
    std::cout << name_of_data_file      << '\t';
    std::cout << number_of_particles    << '\t';
    std::cout << total_defect_particles << '\t';
    std::cout << clusters_bad.size()    << '\t';
    
    if(!clusters_bad.empty())
    {
        double average = double(total_defect_particles)/double(clusters_bad.size());
        double stdev   = sqrt(double(sum_of_squares)/double(clusters_bad.size()) - average*average);
        
        std::cout << (clusters_bad. back()).size() << '\t';  // SIZE OF SMALLEST DEFECT CLUSTER
        std::cout << (clusters_bad.front()).size() << '\t';  // SIZE OF LARGEST DEFECT CLUSTER
        std::cout << average                       << '\t';  // AVERAGE SIZE OF CLUSTER
        std::cout << stdev                         << '\t';  // STANDARD DEVIATION OF CLUSTER SIZE
    }
    else
        std::cout << 0 << '\t' << 0 << '\t' << 0 << '\t' << 0 << '\t';
    
    for(unsigned int c=1; c<max_cluster_size; c++)       // NUMBER OF CLUSTERS WITH 1, 2, 3, ..., max_cluster_size-1 PARTICLES
        std::cout << c_sizes[c] << '\t';
    
    std::cout << '\n';
    std::cout << "We have " << typeA_vacancy_count << " type A vacncies\n";
    std::cout << "We have " << typeB_vacancy_count << " type B vacncies\n";
    std::cout << "We have " << typeC_vacancy_count << " type C vacncies\n";
    std::cout << "We have " << dislocation_count << " dislocations\n";
    std::cout << "We have " << grain_boundary_count << " grain boundaries\n";
    std::cout << '\n';
}



////////////////////////////////////////////////////
////
////   COMPUTES DISTRIBUTION OF TYPES IN SYSTEM
////   AFTER RANDOM GAUSSIAN PERTURBATIONS.
////   COMPUTES perturbation_samples RANDOM
////   PERTURBATIONS OF SYSTEM, EACH OF STANDARD
////   DEVIATION perturbation_size, AND SUMS THE
////   DISTRIBUTION OVER ALL SYSTEMS.  THIS CAN
////   BE USEFUL IN COMPUTING TYPES ASSOCIATED TO
////   REALISTIC VERSIONS OF IDEAL SYSTEMS.
////
////////////////////////////////////////////////////

void calc_gaussian_distribution(voro::container_2d& con, pFilter &pfilter)
{
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0., perturbation_size);
    
    for(int loop = 0; loop < perturbation_samples; loop++)
    {
        // NEW, PERTURBED VERSION OF ORIGINAL SYSTEM
        voro::container_2d con_perturbed
        (origin[0],hi_bound[0],origin[1],hi_bound[1],
         n_x,n_y,true,true,8,threads);
        
        for(int c=0; c<number_of_particles; c++)
        {
            double x = particle_x_coordinates[c] + distribution(generator);
            double y = particle_y_coordinates[c] + distribution(generator);
            con_perturbed.put(c,x,y);
        }
        
        calc_neighbors   (con_perturbed);
        calc_all_pvectors();
        
        for(int c=0; c<number_of_particles; c++)
            pfilter.increment_or_add(all_pvectors[c],1);
    }
    
    pfilter.relabel_data_types();
}



void calc_pstructure_types(pFilter &pfilter)
{
    for(int c=0; c<number_of_particles; c++)
        vt_structure_types[c] = pfilter.pvector_type(all_pvectors[c]);
}


