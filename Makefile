########################################################
####                                                ####
####   ******************************************   ####
####   *                                        *   ####
####   *     VoroTop: Voronoi Cell Topology     *   ####
####   *   Visualization and Analysis Toolkit   *   ####
####   *             (Version 0.3)              *   ####
####   *                                        *   ####
####   *           Emanuel A. Lazar             *   ####
####   *      University of Pennsylvania        *   ####
####   *           December 5, 2014             *   ####
####   *                                        *   ####
####   ******************************************   ####
####                                                ####
########################################################

####   File: Makefile

# C++ compiler
CXX      = g++ -std=c++11 -fopenmp

# Flags for the C++ compiler
LFLAGS   = -lvoro++
CXXFLAGS = -Wall -O3 -c  


vorotop : vorotop.o pvectors.o variables.o filters.o import.o functions.o analysis.o output.o 
	$(CXX) import.o vorotop.o pvectors.o variables.o filters.o functions.o analysis.o output.o $(LFLAGS) -o VoroTop2D

vorotop.o : vorotop.cc filters.hh variables.hh functions.hh
	$(CXX) $(CXXFLAGS) vorotop.cc

pvectors.o : pvectors.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) pvectors.cc

variables.o : variables.cc
	$(CXX) $(CXXFLAGS) variables.cc

filters.o : filters.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) filters.cc

import.o : import.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) import.cc

functions.o : functions.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) functions.cc

analysis.o : analysis.cc filters.hh variables.hh
	$(CXX) $(CXXFLAGS) analysis.cc

output.o : output.cc filters.hh variables.hh 
	$(CXX) $(CXXFLAGS) output.cc

clean :
	rm -f *.o 


