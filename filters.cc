////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: filters.cc


// A FILTER IS A SPECIFICATION OF TOPOLOGICAL TYPES ASSOCIATED
// TO PARTICULAR LOCAL STRUCTURE TYPES.  EACH TOPOLOGICAL TYPE
// IS RECORDED WITH A SPECIAL CODE.  IF WE KNOW WHICH TOPOLOGICAL
// TYPES ARE ASSOCIATED WITH A GIVEN BULK CRYSTAL, THEN PARTICLES
// ASSOCIATED TO DEFECTS CAN BE IDENTIFIED AS THOSE WITH OTHER
// TOPOLOGICAL TYPES.

// VoroTop2D ALLOWS A USER TO SPECIFY A PARTICULAR FILTER TO STUDY
// DATA.  FOR EXAMPLE, WHEN STUDYING HEXAGONAL CRYSTALS, ONE MIGHT
// USE A FILTER SPECIFYING ALL TOPOLOGICAL TYPES ASSOCIATED WITH
// DEFECTS SUCH AS VACANCIES AND DISLOATIONS.

// FILTER FILES FOR COMMON STRUCTURES CAN BE FOUND ONLINE, AT
//    https://www.vorotop.org

// FILTER FILES ARE ORGANIZED AS FOLLOWS:
//
//   LINES BEGINNING WITH # ARE IGNORED AND ARE USED FOR COMMENTS.
//
//   LINES BEGINNING WITH * SPECIFICY STRUCTURE TYPES IN THE FITLER.
//   EACH SUCH LINE, AFTER THE *, INCLUDES AN INTEGER AND THEN A PLAIN-
//   TEXT DESCRIPTION OF THE TYPE.  STRUCTURE TYPES ARE ALWAYS LISTED
//   IN INCREASING ORDER, AND BEGIN COUNTING FROM 1.
//
//   REMAINING LINES RECORD WEINBERG CODES AND ASSOCIATED STRUCTURE
//   TYPES.  EACH LINE BEGINS WITH AN INTEGER AND IS FOLLOWED BY A
//   SEQUENCE OF INTEGERS (A WEINBERG CODE) REPRESENTING A TOPOLOGICAL
//   TYPE.  THE FORMAT OF THE WEINBERG CODES IS THAT FOUND IN THE WORK
//   OF WEINBERG; SEE DOCUMENTATION FOR REFERENCES TO APPROPRIATE WORK.


#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <algorithm>

#include "filters.hh"
#include "variables.hh"




////////////////////////////////////////////////////
////
////   IMPORTS pFILTER FROM FILE
////
////////////////////////////////////////////////////

////   THREE TYPES OF LINES:
////   1) COMMENTS, BEGIN WITH #
////   2) SPECIFICATION OF STRUCTURE TYPES, BEGIN WITH *
////   3) FILTER, STRUCTURE TYPES AND WEINBERG CODES

void pFilter::loadpFilter(std::string filename)
{
    std::ifstream filter_file(filename_pfilter);
    if(!filter_file.is_open())
    {
        std::cerr << "Failed to open input file " << filename_pfilter << "\n\n";
        exit(-1);
    }
    
    structure_types.push_back(std::string());   // STRUCTURE TYPES WILL BE INDEXED BEGINNING FROM 1
    
    std::string line;
    max_file_filter_type = 0;
    int line_counter     = 0;
    
    while ( getline(filter_file, line) )
    {
        std::istringstream is(line);
        
        line_counter++;
        if(line[0] == '#') continue;    // IGNORE COMMENTED LINES
        else if(line[0] == '*')         // ADD STRUCTURE TYPES
        {
            int index;
            std::string name;
            
            is.ignore();    // IGNORES *
            
            is >> index;    // READ STRUCTURE TYPE INDEX
            if(index != ++max_file_filter_type)
            {
                std::cout << "Invalid filter provided.  Structure types in filter must \n";
                std::cout << "be ordered consecutively beginning from 1.\n";
                exit(-1);
            }
            
            // READ STRUCTURE TYPE NAME UP TO NEXT TAB
            is >> std::ws;  // IGNORE WHITESPACE
            char ch;
            while((ch=is.get())!='\t' && !is.eof())
                name += ch;
            structure_types.push_back(name);
        }
        
        else                         // READ WEINBERG CODES
        {
            int type;
            is >> type;
            
            if(type < 1 || type > max_file_filter_type)
            {
                std::cout << "Invalid filter provided.  Structure type " << type << " on line " << line_counter << " does\n";
                std::cout << "not match any of those specified in filter header.\n";
                exit(-1);
            }
            
            std::vector<int> new_pvector;
            is.ignore(256,'(');
            int next_label;
            while (is >> next_label)
            {
                new_pvector.push_back(next_label);
                if (is.peek() == ',') is.ignore();
                if (is.peek() == ')') break;  // IGNORES DATA AFTER FINAL ')'
            }
            
            entries.insert({std::move(new_pvector), pFilterEntry(type)});
        }
    }
    
    file_filter_types = entries.size();
    max_filter_type   = max_file_filter_type;
}



bool compare_by_count_pvector(std::map<std::vector<int>,pFilterEntry>::iterator a, std::map<std::vector<int>,pFilterEntry>::iterator b)
{
    if(a->second.count == b->second.count)
        return a->first < b->first;
    else return a->second.count > b->second.count;
}



void pFilter::increment_or_add(std::vector<int> pvector, int count)
{
    pvector.resize(pvector.size()-2);       // REMOVES SYMMETRY ORDER AND CHIRALITY
    auto it = entries.find(pvector);
    if (it != entries.end()) it->second.count += count;
    else entries.insert({std::move(pvector), pFilterEntry(0, count, 1)});
}


void pFilter::increment_chiral(std::vector<int> pvector)
{
    int symmetry_order = pvector[pvector.size()-2];
    int chirality = pvector[pvector.size()-1];
    
    pvector.resize(pvector.size()-2);       // REMOVES SYMMETRY ORDER AND CHIRALITY
    auto it = entries.find(pvector);
    if (it != entries.end())
    {
        it->second.count += 1;
        it->second.chirals[chirality+1]++;
    }
    else
    {
        entries.insert({std::move(pvector), pFilterEntry(0, 1, 1, chirality)});
    }
}



/////////////////////////////////////////////////////
////
////   COMPUTES AND PRINTS CHIRAL DISTRIBUTION OF TYPES
////
/////////////////////////////////////////////////////

void pFilter::print_distribution_chiral(std::string filename)
{
    std::string distribution_name(filename);
    distribution_name.append(".distribution");
    std::ofstream distribution_file;
    distribution_file.open(distribution_name.c_str());
    
    // WE WOULD LIKE TO ESTIMATE THE SUM OF THE PROBABILITIES
    // OF THE TYPES SAMPLED SO FAR. THIS REQUIRES SOME ANALYSIS.
    double est = 0.;
    for(auto it = entries.begin(); it != entries.end(); ++it) if(it->second.count > 0)
    {
        double pne = double(it->second.count)/number_of_particles;
        est += pne*pow(1.-pne,number_of_particles);
    }
    
    
    // OUTPUT INFORMATION ABOUT SOURCE OF DISTRIBUTION
    distribution_file << "#\tDISTRIBUTION CREATED FROM ";
    if(g_switch==1) distribution_file << "PERTURBATIONS OF ";
    distribution_file << filename;
    if(g_switch==1) distribution_file << ", USING " << perturbation_samples << " PERTURBATIONS WITH MAGNITUDE " << perturbation_size;
    distribution_file << '\n';
    if(d_switch==1 || g_switch==1)
        distribution_file << "#\tEstimated fraction covered: " << 1. - est << '\n';
    
    if(f_switch)    distribution_file << "#\tTypes 1 through " << max_file_filter_type << " obtained from filter in file " << filename_pfilter << '\n';
    
    if((max_filter_type > max_file_filter_type) && d_switch)
    {
        if(f_switch) distribution_file << "#\tTypes " << max_file_filter_type+1 << " through " << max_filter_type << " obtained from other types in data\n";
        else         distribution_file << "#\tTypes " << max_file_filter_type+1 << " through " << max_filter_type << " obtained from types in data\n";
    }
    
    distribution_file << "#\tColumns indicate: type, Weinberg vector, and count\n";
    
    
    // SORT BY COUNT, THEN BY PVECTOR
    std::vector<std::map<std::vector<int>,pFilterEntry>::iterator> sorted_entries;
    for (auto it = entries.begin(); it != entries.end(); ++it)
        sorted_entries.push_back(it);
    std::sort(sorted_entries.begin(), sorted_entries.end(), compare_by_count_pvector);
    
    // OUTPUT THE SORTED DISTRIBUTION
    for(auto it = sorted_entries.begin(); it != sorted_entries.end(); ++it) //if((*it)->second.chirals[1]==0)
    {
        distribution_file << (*it)->second.type << '\t';
        distribution_file << '(';
        for(unsigned int i=0; i<(*it)->first.size()-1; i++)
            distribution_file << (*it)->first[i] << ',';
        distribution_file << (*it)->first.back() << ')' << '\t';
        distribution_file << (*it)->second.count << '\t';
        distribution_file << (*it)->second.chirals[0] << '\t';
        distribution_file << (*it)->second.chirals[1] << '\t';
        distribution_file << (*it)->second.chirals[2] << '\n';
    }
}



////////////////////////////////////////////////////
////
////   COMPUTES AND PRINTS DISTRIBUTION OF TYPES
////
////////////////////////////////////////////////////

void pFilter::print_distribution(std::string filename)
{
    std::string distribution_name(filename);
    distribution_name.append(".distribution");
    std::ofstream distribution_file;
    distribution_file.open(distribution_name.c_str());
    
    // WE WOULD LIKE TO ESTIMATE THE SUM OF THE PROBABILITIES
    // OF THE TYPES SAMPLED SO FAR. THIS REQUIRES SOME ANALYSIS.
    double est = 0.;
    for(auto it = entries.begin(); it != entries.end(); ++it) if(it->second.count > 0)
    {
        double pne = double(it->second.count)/number_of_particles;
        est += pne*pow(1.-pne,number_of_particles);
    }
    
    
    // OUTPUT INFORMATION ABOUT SOURCE OF DISTRIBUTION
    distribution_file << "#\tDISTRIBUTION CREATED FROM ";
    if(g_switch==1) distribution_file << "PERTURBATIONS OF ";
    distribution_file << filename;
    if(g_switch==1) distribution_file << ", USING " << perturbation_samples << " PERTURBATIONS WITH MAGNITUDE " << perturbation_size;
    distribution_file << '\n';
    if(d_switch==1 || g_switch==1)
        distribution_file << "#\tEstimated fraction covered: " << 1. - est << '\n';
    
    if(f_switch)    distribution_file << "#\tTypes 1 through " << max_file_filter_type << " obtained from filter in file " << filename_pfilter << '\n';
    
    if((max_filter_type > max_file_filter_type) && d_switch)
    {
        if(f_switch) distribution_file << "#\tTypes " << max_file_filter_type+1 << " through " << max_filter_type << " obtained from other types in data\n";
        else         distribution_file << "#\tTypes " << max_file_filter_type+1 << " through " << max_filter_type << " obtained from types in data\n";
    }
    
    distribution_file << "#\tColumns indicate: type, Weinberg vector, and count\n";
    
    
    // SORT BY COUNT, THEN BY PVECTOR
    std::vector<std::map<std::vector<int>,pFilterEntry>::iterator> sorted_entries;
    for (auto it = entries.begin(); it != entries.end(); ++it)
        sorted_entries.push_back(it);
    std::sort(sorted_entries.begin(), sorted_entries.end(), compare_by_count_pvector);
    
    // OUTPUT THE SORTED DISTRIBUTION
    for(auto it = sorted_entries.begin(); it != sorted_entries.end(); ++it) if((*it)->second.count > 0)
    {
        distribution_file << (*it)->second.type << '\t';
        distribution_file << '(';
        for(unsigned int i=0; i<(*it)->first.size()-1; i++)
            distribution_file << (*it)->first[i] << ',';
        distribution_file << (*it)->first.back() << ')' << '\t';
        distribution_file << (*it)->second.count << '\n';
    }
}



////////////////////////////////////////////////////
////
////   FINDS FILTER NUMBER ASSOCIATED WITH A CELL
////
////////////////////////////////////////////////////
int pFilter::pvector_type(std::vector<int> pvector)
{
    auto it = entries.find(pvector);
    if (it != entries.end()) return it->second.type;
    else                     return 0;
}



////////////////////////////////////////////////////
////
////   RELABELS TYPES OBTAINED FROM DATA IN
////   DECREASING ORDER OF FREQUENCY
////
////////////////////////////////////////////////////

void pFilter::relabel_data_types(void)
{
    std::vector<std::map<std::vector<int>,pFilterEntry>::iterator> sorted_entries;
    
    for (auto it = entries.begin(); it != entries.end(); ++it) if(it->second.source == 1)
        sorted_entries.push_back(it);
    std::sort(sorted_entries.begin(), sorted_entries.end(), compare_by_count_pvector);
    
    for(auto it = sorted_entries.begin(); it != sorted_entries.end(); ++it)
        (*it)->second.type = ++max_filter_type;
}

