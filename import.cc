////////////////////////////////////////////////////////
////                                                ////
////   ******************************************   ////
////   *                                        *   ////
////   *    VoroTop2D: Voronoi Cell Topology    *   ////
////   *   Visualization and Analysis Toolkit   *   ////
////   *          in Two Dimensions             *   ////
////   *             (Version 1.0)              *   ////
////   *                                        *   ////
////   *           Emanuel A. Lazar             *   ////
////   *         Bar-Ilan University            *   ////
////   *            September 2022              *   ////
////   *                                        *   ////
////   ******************************************   ////
////                                                ////
////////////////////////////////////////////////////////

////   File: import.cc


#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>

#include "filters.hh"
#include "variables.hh"


// TAKEN FROM https://stackoverflow.com/questions/41045236
// TO FACILITATE IGNORING DATA IN FILE
template <typename CharT>
std::basic_istream<CharT>& ignoreM(std::basic_istream<CharT>& in){
    std::string ignoredValue;
    return in >> ignoredValue;
}


////////////////////////////////////////////////////
////
////   PARSE HEADER OF INPUT FILE IN DUMP FORMAT.
////
////////////////////////////////////////////////////

void parse_header(std::ifstream& fp)
{
    ////////////////////////////////////////////////////
    ////
    ////    INITIALIZE VARIABLES
    ////
    ////////////////////////////////////////////////////
    
    origin[0]=0.;
    origin[1]=0.;
    for(int c=0; c<2; c++)
        for(int d=0; d<2; d++)
            supercell_edges[c][d]=0;
    
    
    ////////////////////////////////////////////////////
    ////
    ////    DETERMINE FILE TYPE
    ////
    ////////////////////////////////////////////////////
    
    std::string full_line;
    getline(fp, full_line);
    
    if (full_line.find("ITEM: TIMESTEP") != std::string::npos)
        header_lines=2;
    
    else
    {
        std::cout << "Unrecognized data file format\n";
        exit(0);
    }
    

    ////////////////////////////////////////////////////
    ////
    ////    PARSE FILE
    ////
    ////////////////////////////////////////////////////
    
    bool done=0;
    
    while(done==0)
    {
        getline(fp, full_line);
        std::istringstream iss(full_line);
        
        if (full_line.find("ITEM: NUMBER OF ATOMS") != std::string::npos)
        {
            fp >> number_of_particles;
            header_lines+=2;
        }
        
        else if (full_line.find("ITEM: BOX BOUNDS pp pp pp") != std::string::npos)          // PERIODIC IN ALL THREE DIRECTIONS
        {
            for(int c=0; c<2; c++)
            {
                fp >> origin[c];
                fp >> hi_bound[c];
                supercell_edges[c][c] = hi_bound[c]-origin[c];
            }
            fp >> ignoreM;
            fp >> ignoreM;
            
            header_lines+=4;
        }
        
        else if (full_line.find("ITEM: ATOMS ") != std::string::npos)
        {
            // DETERMINE ATOM ATTRIBUTES, INDEX OF COORDINATES, AND SCALING
            std::string entry;
            iss >> entry;   // READS IN "ITEM:"
            iss >> entry;   // READS IN "ATOMS"
            
            while(iss >> entry)                             // READ ATTRIBUTE LABELS, I.E., 'id', 'type', 'x', 'y', and 'z'
                attribute_labels.push_back(entry);
            
            xindex=-1;
            for(unsigned int c=0; c<attribute_labels.size(); c++)
            {
                if(attribute_labels[c]=="x" || attribute_labels[c]=="xs")
                {
                    if(attribute_labels[c]=="x") scaled_coordinates=0;
                    else                         scaled_coordinates=1;
                    
                    attribute_labels.erase(attribute_labels.begin()+c, attribute_labels.begin()+c+3);
                    xindex=c;
                }
            }
            
            if(xindex==-1)
            {
                std::cout << "Insufficient xyz coordinates included in dump file.\n";
                exit(-1);
            }
            
            header_lines+=1;
            done=1;
        }
    }

    // IF particles_in_eps WAS SET INITIALLY TO 0, THEN WE WANT TO
    // DRAW ALL PARTICLES
    if(particles_in_eps==0) particles_in_eps=number_of_particles;
    int total_blocks = number_of_particles/8;
    double area = supercell_edges[0][0]*supercell_edges[1][1];  // APPROXIMATION
    double area_per_block = area/(double)total_blocks;
    double length_per_rectangle = pow(area_per_block, 1./2.);
    
    n_x = int(supercell_edges[0][0]/length_per_rectangle+1.);
    n_y = int(supercell_edges[1][1]/length_per_rectangle+1.);
}



////////////////////////////////////////////////////
////
////   IMPORTS DATA POINTS FROM DUMP FILE
////
////////////////////////////////////////////////////

void import_data(std::ifstream& fp, voro::container_2d &con)
{
    for(int c=0; c<number_of_particles; c++)
    {
        double x,y;

        for(int d=0; d<xindex; d++)
            fp >> ignoreM;
        
        fp >> x >> y;
        particle_x_coordinates[c]=x;
        particle_y_coordinates[c]=y;
        
        fp >> ignoreM;      // STORES Z COORDINATE, WHICH IS IGNORED
        for(unsigned int d=xindex+3; d<attribute_labels.size()+3; d++)
            fp >> ignoreM;
        
        // ADJUST COORDINATES SO SYSTEM UNSCALED AND AT ORIGIN
        if(scaled_coordinates==0)
        {
            x -= origin[0];
            y -= origin[1];
        }
        
        if(scaled_coordinates==1)
        {
            double newx = supercell_edges[0][0]*x + supercell_edges[1][0]*y;
            double newy = supercell_edges[0][1]*x + supercell_edges[1][1]*y;
            
            x=newx;
            y=newy;
        }

        con.put(c,x,y);
    }
}


